import LayoutBasic from "../components/LayoutBasic.js/LayoutBasic"
import Login from "../components/Login"
import Home from "../pages/Home"
import OpenCorporateCustomer from "../pages/TellerOperation/CustomerManagement/OpenCorporateCustomer"
import OpenIndividualCustomer from "../pages/TellerOperation/CustomerManagement/OpenIndividualCustomer"


// Public Routes
const publicRoutes = [
    { path: '/login', component: Login},
]

// Private Routes
const privateRoutes = [
    // { path: '/login', component: LoginPage},
    { path: '/login', component: Login},

    { path: '/', component: Home},
    { path: '/open_individual_customer', component: OpenIndividualCustomer},
    { path: '/open_corporate_customer', component: OpenCorporateCustomer},


    
]


export { publicRoutes, privateRoutes }