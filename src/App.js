import logo from './logo.svg';
import './App.css';
import LayoutBasic from './components/LayoutBasic.js/LayoutBasic';
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import { privateRoutes } from "./routes";
import LoginLayout from './components/layouts/LoginLayout';

function App() {
  return (
    <div className="App">
        {/* <LayoutBasic /> */}
        <Router>
        <Routes>
        {
          privateRoutes.map((route, index)=> {
            const Page = route.component
            let Layout;
            if (route.path === "/login") {
              Layout = LoginLayout
            } else {
              Layout = LayoutBasic
            }
            return <Route key={index} path={route.path} element={
              <Layout>
                  <Page />
              </Layout>} />
          })
        }
        </Routes>
    </Router>
    </div>
  );
}

export default App;
